/*
The client main file for box version 2

Uses RPC to communicate with a server serving box version 2.

Compile: gcc box_2_client_main.c box_2_clnt.c box_2_xdr.c -o client
NOTE: For arch linux use additional "-I/usr/include/tirpc -ltirpc" flags
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "box_2.h"

#define LN_SEP "----------------------------\n"


void print_box(float l, float w, float h, float sa, float v);
void print_mail_params(float v, float m, float p);


/*
Main function

Required arguments: remote_host length width height mass
        remote_host - The remote RPC server host
        length - the length of the box
        width - the width of the box
        height - the height of the box
        mass - the mass of the box
*/
int main (int ac, char *av[])
{
        CLIENT *client;
        dimensions box_dimensions;
        mail_dims mail_dimensions;
        box_results *res;
        float *cost;

        if (ac < 6)
        {
                printf("Use: %s remote_host length width height mass\n", av[0]);
                exit(EXIT_FAILURE);
        }

        box_dimensions.length = atof(av[2]);
        box_dimensions.width = atof(av[3]);
        box_dimensions.height = atof(av[4]);
        mail_dimensions.mass = atof(av[5]);

        client = clnt_create(av[1], RPC_BOX, BOXVERSION2, "udp");
        if (client == NULL)
        {
                clnt_pcreateerror(av[1]);
                exit(EXIT_FAILURE);
        }

        res = box_calc_2(&box_dimensions, client);
        if (res == NULL)
        {
                clnt_perror(client, av[1]);
                exit(EXIT_FAILURE);
        }

        print_box(box_dimensions.length,
                        box_dimensions.width,
                        box_dimensions.height,
                        res->surface,
                        res->volume
        );

        mail_dimensions.volume = res->volume;

        cost = mail_calc_2(&mail_dimensions, client);
        if (cost == NULL)
        {
                clnt_perror(client, av[1]);
                exit(EXIT_FAILURE);
        }

        print_mail_params(mail_dimensions.volume,
                                mail_dimensions.mass,
                                *cost
        );

        clnt_destroy(client);

        return 0;
}


/* Prints the variables provided from the dimensions and box_results structs */
void print_box(float l, float w, float h, float sa, float v)
{
        printf("Box dimensions: %f x %f x %f\n", l, w, h);
        printf("Surface area: %f\n", sa);
        printf("Volume: %f\n", v);
        printf(LN_SEP);
}


/* Prints the cost and parameters of the mail_dims struct */
void print_mail_params(float v, float m, float p)
{
        printf("Volume: %f\n", v);
        printf("Mass: %f\n", m);
        printf("Cost $%f\n", p);
        printf(LN_SEP);
}
