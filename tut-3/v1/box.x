/* The dimensions of a rectangular prism */
struct dimensions
{
        float length;
        float width;
        float height;
};


/* Returned results from the RPC call */
struct box_results
{
        float volume;
        float surface;
};


/* RPC service definition */
program RPC_BOX {
        version BOXVERSION1
        {
                box_results BOX_CALC(dimensions) = 1;
        } = 1;
} = 0x40049453;
