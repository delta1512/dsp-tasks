/*
The server function definition file for box version 1

Calculates surface area and volume.

Compile: gcc box_funcs.c box_svc.c box_xdr.c -o server
NOTE: For arch linux use additional "-I/usr/include/tirpc -ltirpc" flags
*/

#include "box.h"


/* Calculate surface area of a rectangular prism */
float surface(float l, float w, float h)
{
        return 2 * (l*w + l*h + w*h);
}


/* Calculate the volume of a rectangular prism */
float volume(float l, float w, float h)
{
        return l*w*h;
}


/* The RPC service handler */
box_results *box_calc_1_svc(dimensions *dims, struct svc_req *rqstp)
{
        static box_results results;
        results.surface = surface(dims->length, dims->width, dims->height) ;
        results.volume = volume(dims->length, dims->width, dims->height);
        return &results;
}
