/*
The client main file for box version 1

Uses RPC to communicate with a server serving box version 1.

Compile: gcc box_client_main.c box_clnt.c box_xdr.c -o client
NOTE: For arch linux use additional "-I/usr/include/tirpc -ltirpc" flags
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "box.h"

#define LN_SEP "----------------------------\n"


void print_box(float l, float w, float h, float sa, float v);

/*
Main function

Required arguments: remote_host length width height mass
        remote_host - The remote RPC server host
        length - the length of the box
        width - the width of the box
        height - the height of the box
*/
int main (int ac, char *av[])
{
        CLIENT *client;
        dimensions dims;
        box_results *results;

        if (ac < 5)
        {
                printf("Use: %s remote_host length width height\n", av[0]);
                exit(EXIT_FAILURE);
        }

        dims.length = atof(av[2]);
        dims.width = atof(av[3]);
        dims.height = atof(av[4]);

        client = clnt_create(av[1], RPC_BOX, BOXVERSION1, "udp");
        if (client == NULL)
        {
                clnt_pcreateerror(av[1]);
                exit(EXIT_FAILURE);
        }

        results = box_calc_1(&dims, client);
        if (results == NULL)
        {
                clnt_perror(client, av[1]);
                exit(EXIT_FAILURE);
        }

        print_box(dims.length,
                        dims.width,
                        dims.height,
                        results->surface,
                        results->volume
        );

        clnt_destroy(client);

        return 0;
}


/* Prints the variables provided from the dimensions and box_results structs */
void print_box(float l, float w, float h, float sa, float v)
{
        printf("Box dimensions: %f x %f x %f\n", l, w, h);
        printf("Surface area: %f\n", sa);
        printf("Volume: %f\n", v);
        printf(LN_SEP);
}
