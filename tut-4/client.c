/*
  client.c written by Marcus Belcastro (19185398)

  Based on the client from tutorial 2. This client uses sockets to perform
  a HTTP HEAD or GET request on the root directory of a web server.

  Compiling instructions:
    gcc client.c -o client.out
*/

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <netdb.h>
#include <string.h>
#include <unistd.h>

#include <sys/socket.h>

#define TRACE(message)                                  \
        do                                              \
        {                                               \
                fprintf(stderr, "TRACE: ");             \
                fprintf(stderr, message);               \
                fprintf(stderr, "\n");                  \
        }                                               \
        while(0)
/* Buffer lengths */
#define BUF_LEN 1024
#define REQ_BUF_LEN 1024
#define HTTP_HEAD_LEN 4096
#define HTTP_BODY_LEN 65536
#define RESP_BUF_LEN HTTP_HEAD_LEN + HTTP_BODY_LEN
/* RFC definitions */
#define CRLF "\r\n"
#define SP ' '
/* HTTP definitions */
#define HTTP_PORT_NUM 80
#define HEAD_REQ_STR "HEAD / HTTP/1.1\r\n"
#define GET_REQ_STR "GET / HTTP/1.1\r\n"
#define HTTP_OK 200
/* HTTP headers */
#define CONTENT_TYPE_HEADER "Content-Type: "
#define LAST_MODIFIED_HEADER "Last-Modified: "
/* Sentinel values */
#define END_HEAD "\r\n\r\n"
#define END_BODY "</html>"


int read_socket_to_buf(int fd, char *to_buf, int len, char *term);
int parse_http_resp(char *resp_text);


/*
  Client that connects to a web server and requests the root document.

  Commandline parameters: [GET/HEAD] [HOST]
    - GET/HEAD: A literal GET or HEAD to choose a HTTP-GET or HTTP-HEAD request
    - HOST: The hostname (or ip address) of the server
*/
int main(int argc, char *argv[])
{
        int bytes_rw;
        int error_code;
        int client_socket;
        char recv_buf[RESP_BUF_LEN], send_buf[REQ_BUF_LEN], terminator[BUF_LEN];
        struct hostent *server_host;
        struct sockaddr_in server;

        if (argc < 3)
        {
                printf("Invalid arguments, use: [GET/HEAD] [HOST]\n");
                exit(EXIT_FAILURE);
        }

        if (!strcmp(argv[1], "GET"))
        {
                strcpy(send_buf, GET_REQ_STR);
                strcpy(terminator, END_BODY);
        }
        else
        {
                strcpy(send_buf, HEAD_REQ_STR);
                strcpy(terminator, END_HEAD);
        }

        /*
        Host header ensures that modern web servers can understand
        the request. This fixes a response-status 400 issue on some servers.
        */
        strcat(send_buf, "Host: ");
        strcat(send_buf, argv[2]);
        strcat(send_buf, CRLF);
        strcat(send_buf, CRLF);

        server_host = gethostbyname(argv[2]);

        if (!server_host)
        {
                perror("Could not find the host specified.");
                exit(EXIT_FAILURE);
        }

        TRACE("Creating the client socket");

        server.sin_family = AF_INET;
        server.sin_port = htons(HTTP_PORT_NUM);
        memcpy(&server.sin_addr.s_addr,
              server_host->h_addr,
              server_host->h_length);

        client_socket = socket(AF_INET, SOCK_STREAM, 0);

        if (client_socket < 0)
        {
                perror("Failed to create a socket.");
                exit(EXIT_SUCCESS);
        }

        TRACE("Successfully created the socket, now connecting");

        error_code = connect(client_socket,
                            (struct sockaddr*) &server,
                            sizeof(server));

        if (error_code < 0)
        {
                perror("Failed to connect to the socket.");
                exit(EXIT_SUCCESS);
        }

        TRACE("Successfully connected to the socket, sending HEAD request");

        bytes_rw = write(client_socket, send_buf, strlen(send_buf));

        if (bytes_rw < 0)
        {
                perror("Error sending HEAD request");
                close(client_socket);
                exit(EXIT_FAILURE);
        }

        TRACE("Awaiting web server message");

        bytes_rw = read_socket_to_buf(client_socket,
                                     recv_buf,
                                     RESP_BUF_LEN,
                                     terminator);

        if (bytes_rw < 0)
        {
                perror("Error reading from the socket");
                close(client_socket);
                exit(EXIT_FAILURE);
        }

        TRACE("Got data, parsing the response");

        parse_http_resp(recv_buf);

        close(client_socket);

        TRACE("Successfully closed the socket");

        return EXIT_SUCCESS;
}


/*
Reads the data from a socket to a provided buffer.

The function is almost identical to the std read() syscall
but can handle buffers of arbitrary size by writing successive blocks
to the buffer.

NOTE: The terminator is an optional string upon which reading should terminate.
      This is due to an issue where the read() call hangs on the last block.
*/
int read_socket_to_buf(int fd, char *to_buf, int len, char *term)
{
        int bytes_read;
        int total_bytes = 0;
        char read_buf[BUF_LEN];

        memset(to_buf, 0, len);

        do
        {
                bytes_read = read(fd, read_buf, BUF_LEN);

                if (bytes_read < 0)
                {
                        /* Propagate the error back to caller */
                        return bytes_read;
                }

                if (total_bytes + bytes_read > len)
                {
                        /* Return an error to indicate a buffer overflow */
                        return -1;
                }

                strncat(to_buf, read_buf, bytes_read);

                total_bytes += bytes_read;

        } while (bytes_read > 0 && !strstr(to_buf, term));

        return total_bytes;
}


/*
Interprets and prints a HTTP response.

Will only print the status if it is not HTTP_OK, else it will try and print the
body and two specific headers CONTENT_TYPE_HEADER and LAST_MODIFIED_HEADER.

Returns -1 if status != HTTP_OK
else returns 0
*/
int parse_http_resp(char *resp_text)
{
        int status;
        char buff[BUF_LEN];
        char *pos;

        memset(buff, 0, BUF_LEN);

        /* The status code is after the first SP character */
        pos = strchr(resp_text, SP);

        if (pos)
        {
                strncpy(buff, pos, 4);
                status = atoi(buff);

                printf("Status: %d\n", status);

                if (status != HTTP_OK)
                {
                        return 1;
                }
        }
        else
        {
                perror("Bad HTTP response provided.");
                exit(EXIT_FAILURE);
        }

        memset(buff, 0, BUF_LEN);

        pos = strstr(resp_text, CONTENT_TYPE_HEADER);

        if (pos)
        {
                /* Move past the header name */
                pos += strlen(CONTENT_TYPE_HEADER);
                /* Copy until the end of the header value */
                strncpy(buff, pos, (int) (strstr(pos, CRLF) - pos));
                printf("Content-Type: %s\n", buff);
        }

        memset(buff, 0, BUF_LEN);

        pos = strstr(resp_text, LAST_MODIFIED_HEADER);

        if (pos)
        {
                /* Same as above */
                pos += strlen(LAST_MODIFIED_HEADER);
                strncpy(buff, pos, (int) (strstr(pos, CRLF) - pos));
                printf("Last-Modified: %s\n", buff);
        }

        memset(buff, 0, BUF_LEN);

        pos = strstr(resp_text, END_HEAD);

        if (pos)
        {
                /* Move past the double CRLF */
                pos += strlen(END_HEAD);

                if (strlen(pos) > 1)
                {
                        printf("Body: %s\n", pos);
                }
        }

        return 0;
}
