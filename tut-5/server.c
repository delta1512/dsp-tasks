/*
server.c written by Marcus Belcastro (19185398)

Concurrent, connection-oriented HTTP server that responds to
HEAD and GET requests.

Compiling instructions:
        gcc server.c -o server.out
*/

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>
#include <assert.h>
#include <fcntl.h>

#include <netinet/in.h>

#include <sys/wait.h>
#include <sys/types.h>
#include <sys/socket.h>


#define TRACE(message)                                  \
        do                                              \
        {                                               \
                fprintf(stderr, "TRACE %d: ", getpid());\
                fprintf(stderr, message);               \
                fprintf(stderr, "\n");                  \
        }                                               \
        while(0)
#define SERVER_PORT 5398
#define CONN_BACKLOG 16
#define BUF_LEN 2048
#define DATA_FILE "oranges_and_lemons.txt"
#define NUM_LINES 50
#define HTTP_SERVER_VERSION "1.0"
/* HTTP headers */
#define CONTENT_TYPE "Content-Type: text/plain"
#define CONTENT_LENGTH "Content-Length: "
/* RFC definitions */
#define CRLF "\r\n"
#define CRLFCRLF "\r\n\r\n"
#define SP " "
#define LF '\n'
/* HTTP status codes and reason phrases */
#define OK 200
#define OK_CODE_AND_REASON "200 OK"
#define NOT_FOUND 404
#define NOT_FOUND_CODE_AND_REASON "404 Not Found"
#define NOT_IMP 501
#define NOT_IMP_CODE_AND_REASON "501 Not Implemented"
#define BAD_REQ 400
#define BAD_REQ_CODE_AND_REASON "400 Bad Request"


void child_handler(int signum);
void process_connection(int client_socket);
void get_resp_text(char *buf);
int check_request(char *req_str);


/*
HTTP server that responds to GET and HEAD requests.

No commandline parameters are needed.
*/
int main(int argc, char *argv[])
{
        int server_socket;      /* The socket descriptor of this server */
        int handler_socket;     /* The socket descriptor for communicating with client */
        int addr_len;           /* The length of a sockaddr */
        int error_code;         /* Storage for error codes returned by syscalls */
        int fork_pid;           /* The resulting PID from fork() */
        struct sigaction child_sig_act; /* The signal action for SIGCHLD */
        struct sockaddr_in server_addr; /* Address structure of the server */
        struct sockaddr_in client_addr; /* Address structure of the client */

        sigfillset(&child_sig_act.sa_mask);
        child_sig_act.sa_handler = child_handler;
        child_sig_act.sa_flags = SA_RESTART | SA_NOCLDSTOP;
        sigaction(SIGCHLD, &child_sig_act, NULL);

        server_addr.sin_family = AF_INET;
        server_addr.sin_port = htons(SERVER_PORT);
        server_addr.sin_addr.s_addr = htonl(INADDR_ANY);

        TRACE("Creating server socket");

        server_socket = socket(AF_INET, SOCK_STREAM, 0);

        if (server_socket < 0)
        {
                perror("Failed to create the server socket");
                exit(EXIT_FAILURE);
        }

        TRACE("Successfully created socket, binding server address");

        error_code = bind(server_socket,
                         (struct sockaddr*) &server_addr,
                         sizeof(server_addr));

        if (error_code < 0)
        {
                perror("Failed to bind address to socket");
                exit(EXIT_FAILURE);
        }

        TRACE("Successfully bound address, listening for connections");

        error_code = listen(server_socket, CONN_BACKLOG);

        if (error_code < 0)
        {
                perror("Failed to begin listening for connections");
                exit(EXIT_FAILURE);
        }

        TRACE("Successfully began listening, now accepting connections");

        /* Main server loop */
        while (1)
        {
                TRACE("Waiting for a connection");

                handler_socket = accept(server_socket,
                                       (struct sockaddr*) &client_addr,
                                       &addr_len);

                if (handler_socket < 0)
                {
                        perror("Failed to accept this connection");
                        exit(EXIT_FAILURE);
                }

                TRACE("Successfully accepted connection, forking new process");

                fork_pid = fork();

                switch (fork_pid)
                {
                case -1:
                        perror("Failed to fork a new process");
                        exit(EXIT_FAILURE);
                        break;
                case 0:
                        TRACE("Child process close old socket and process msg");
                        close(server_socket);
                        process_connection(handler_socket);
                        close(handler_socket);
                        exit(EXIT_SUCCESS);
                default:
                        TRACE("Parent process, close new socket and continue");
                        close(handler_socket);
                        break;
                }
        }

        return EXIT_SUCCESS;
}


/*
Manage a connection that the server encounters.

This function must be called from a child process.

handler_socket is the socket descriptor returned by the accept() system call.
handler_socket must be closed from outside this function.
*/
void process_connection(int handler_socket)
{
        int bytes_rw;
        int buf_start;
        int rts;
        char first_char;
        char in_buf[BUF_LEN], out_buf[BUF_LEN], data[BUF_LEN], data_len[5];

        read(handler_socket, in_buf, BUF_LEN);

        strcpy(out_buf, "HTTP/");
        strcat(out_buf, HTTP_SERVER_VERSION);
        strcat(out_buf, SP);

        switch (check_request(in_buf))
        {
        case 1:
                TRACE("HEAD request encountered, sending headers...");
                /*
                RFC states that we should still include the
                Content-Length header. No body wil be appended.
                */
                get_resp_text(data);
                sprintf(data_len, "%d", strlen(data));
                strcat(out_buf, OK_CODE_AND_REASON);
                strcat(out_buf, CRLF);
                strcat(out_buf, CONTENT_TYPE);
                strcat(out_buf, CRLF);
                strcat(out_buf, CONTENT_LENGTH);
                strcat(out_buf, data_len);
                strcat(out_buf, CRLFCRLF);
                break;
        case 2:
                TRACE("GET request encountered, sending data...");
                get_resp_text(data);
                sprintf(data_len, "%d", strlen(data));
                strcat(out_buf, OK_CODE_AND_REASON);
                strcat(out_buf, CRLF);
                strcat(out_buf, CONTENT_TYPE);
                strcat(out_buf, CRLF);
                strcat(out_buf, CONTENT_LENGTH);
                strcat(out_buf, data_len);
                strcat(out_buf, CRLFCRLF);
                strcat(out_buf, data);
                break;
        case NOT_IMP:
                TRACE("Bad method requested");
                strcat(out_buf, NOT_IMP_CODE_AND_REASON);
                strcat(out_buf, CRLFCRLF);
                break;
        case NOT_FOUND:
                TRACE("Bad URL requested");
                strcat(out_buf, NOT_FOUND_CODE_AND_REASON);
                strcat(out_buf, CRLFCRLF);
                break;
        case BAD_REQ:
                TRACE("Bad request encountered");
                strcat(out_buf, BAD_REQ_CODE_AND_REASON);
                strcat(out_buf, CRLFCRLF);
                break;
        }

        write(handler_socket, out_buf, strlen(out_buf));

        TRACE("Server successfully processed a connection");
}


/*
Takes a random number of lines from oranges and lemons and prints it to the buf.

buf is expected to be of size BUF_LEN.

The algorithm copies the data char-by-char and if there is a LF char, decrement
the counter.
*/
void get_resp_text(char *buf)
{
        int i = 0;
        int lines_to_print;
        int data_fd = open(DATA_FILE, O_RDONLY);
        char file_buf[BUF_LEN];

        srand(time(NULL));

        /* Number between 1 and NUM_LINES */
        lines_to_print = (rand() % NUM_LINES) + 1;

        memset(buf, 0, BUF_LEN);

        read(data_fd, file_buf, BUF_LEN);

        while (lines_to_print > 0)
        {
                if (file_buf[i] == LF)
                {
                        lines_to_print--;
                }

                buf[i] = file_buf[i];

                i++;
        }

        close(data_fd);
}


/*
Checks whether a HTTP request is valid and returns the type of request or error.
This function does not do a rigorous check and does not check headers.

A valid HTTP HEAD request returns 1
A valid HTTP GET request returns 2
Requesting non-root URL returns NOT_FOUND
Unimplemented functionality requested returns NOT_IMP
Else BAD_REQ
*/
int check_request(char *req_str)
{
        int major_version;
        int minor_version;
        int ret_val = BAD_REQ;
        char *read_ptr = req_str;

        if (strncmp(req_str, "HEAD", 4))
        {
                if (strncmp(req_str, "GET", 3))
                {
                        return NOT_IMP;
                }
                else
                {
                        ret_val = 2;
                        read_ptr += 3;
                }
        }
        else
        {
                ret_val = 1;
                read_ptr += 4;
        }

        if (strncmp(read_ptr, " / ", 3))
        {
                return NOT_FOUND;
        }

        read_ptr += 3;

        if (strncmp(read_ptr, "HTTP/", 5))
        {
                return BAD_REQ;
        }

        read_ptr += 5;

        /* 48 is '0' in ASCII. This converts the character to an int */
        major_version = (*read_ptr) - 48;
        if (!(major_version == 1 || major_version == 2))
        {
                return BAD_REQ;
        }

        if ((*(++read_ptr)) != '.')
        {
                return BAD_REQ;
        }

        /* This server supports any 1.* and 2.* so we check for any minor */
        minor_version = (*(++read_ptr)) - 48;
        if (minor_version < 0 || minor_version > 9)
        {
                return BAD_REQ;
        }

        return ret_val;
}


/*
Signal handler for SIGCHLD

Collects all child statuses and does nothing with them
in order to prevent zombie processes.
*/
void child_handler(int signum)
{
        int child_pid;

        do
        {
                TRACE("Collecting children");
                child_pid = waitpid(-1, NULL, WNOHANG);
        }
        while (child_pid > 0);
}
