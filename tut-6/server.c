/*
server.c written by Marcus Belcastro (19185398)

Concurrent, connection-oriented server that modifies some provided text.
When receiving text, the server will lower any uppercase alphabetic characters
and ommit non-alphabetic characters.

This program only supports IPv6 and is not protocol-agnostic

Compiling instructions:
        gcc server.c -o server.out
*/

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>
#include <assert.h>
#include <netdb.h>

#include <sys/wait.h>
#include <sys/types.h>
#include <sys/socket.h>


#define TRACE(message)                                  \
        do                                              \
        {                                               \
                fprintf(stderr, "TRC %d: ", getpid());  \
                fprintf(stderr, message);               \
                fprintf(stderr, "\n");                  \
        }                                               \
        while(0)
#define CONN_BACKLOG 16
#define BUF_LEN 1024
#define END_SESSION_CHAR 'X'
#define END_DATA_CHAR '&'


void child_handler(int signum);
void make_lower_alphabetic_only(char *input_str, char *output_str);
void process_connection(int client_socket);
int process_client_data(char *in, int in_len, char *out, int out_len, int offs);


/*
  Server that concurrently modifies messages for multiple clients over TCP.
  This server supports IPv6 only

  Commandline parameters: [PORT]
    - PORT: The port that the server will listen on
*/
int main(int argc, char *argv[])
{
        int server_socket;      /* The socket descriptor of this server */
        int handler_socket;     /* The socket descriptor for communicating with client */
        int addr_len;           /* The length of a sockaddr */
        int error_code;         /* Storage for error codes returned by syscalls */
        int fork_pid;           /* The resulting PID from fork() */
        char hostname[BUF_LEN]; /* The hostname of the connecting client */
        unsigned short server_port;      /* The TCP port that the server runs on */
        struct sigaction child_sig_act;  /* The signal action for SIGCHLD */
        struct sockaddr_in6 server_addr; /* Address structure of the server */
        struct sockaddr_in6 client_addr; /* Address structure of the client */

        sigfillset(&child_sig_act.sa_mask);
        child_sig_act.sa_handler = child_handler;
        child_sig_act.sa_flags = SA_RESTART | SA_NOCLDSTOP;
        sigaction(SIGCHLD, &child_sig_act, NULL);

        if (argc < 2)
        {
                printf("Invalid arguments, use: [PORT]\n");
                exit(EXIT_FAILURE);
        }

        server_port = atoi(argv[1]);

        server_addr.sin6_family = AF_INET6;
        server_addr.sin6_port = htons(server_port);
        server_addr.sin6_addr = in6addr_any;
        server_addr.sin6_flowinfo = 0;

        TRACE("Creating server socket");

        server_socket = socket(AF_INET6, SOCK_STREAM, 0);

        if (server_socket < 0)
        {
                perror("Failed to create the server socket");
                exit(EXIT_FAILURE);
        }

        TRACE("Successfully created socket, binding server address");

        error_code = bind(server_socket,
                         (struct sockaddr*) &server_addr,
                         sizeof(server_addr));

        if (error_code < 0)
        {
                perror("Failed to bind address to socket");
                exit(EXIT_FAILURE);
        }

        TRACE("Successfully bound address, listening for connections");

        error_code = listen(server_socket, CONN_BACKLOG);

        if (error_code < 0)
        {
                perror("Failed to begin listening for connections");
                exit(EXIT_FAILURE);
        }

        TRACE("Successfully began listening, now accepting connections");

        while (1)
        {
                TRACE("Waiting for a connection");

                handler_socket = accept(server_socket,
                                       (struct sockaddr*) &client_addr,
                                       &addr_len);

                if (handler_socket < 0)
                {
                        perror("Failed to accept this connection");
                        exit(EXIT_FAILURE);
                }


                error_code = getnameinfo((struct sockaddr*) &client_addr,
                                        sizeof(client_addr),
                                        hostname,
                                        BUF_LEN,
                                        NULL,
                                        0,
                                        NI_NAMEREQD);

                switch (error_code)
                {
                case 0:
                        TRACE("Host connected with name: ");
                        fprintf(stderr, "%s\n", hostname);
                        break;
                case EAI_NONAME:
                        TRACE("Host without a hostname connected");
                        break;
                default:
                        perror("Failed name resolution for host, exiting...");
                        exit(EXIT_FAILURE);
                        break;
                }

                TRACE("Successfully accepted connection, forking new process");

                fork_pid = fork();

                switch (fork_pid)
                {
                case -1:
                        perror("Failed to fork a new process");
                        exit(EXIT_FAILURE);
                        break;
                case 0:
                        TRACE("Child process close old socket and process msg");
                        close(server_socket);
                        process_connection(handler_socket);
                        close(handler_socket);
                        exit(EXIT_SUCCESS);
                        break;
                default:
                        TRACE("Parent process, close new socket and continue");
                        close(handler_socket);
                        break;
                }
        }

        return EXIT_SUCCESS;
}


/*
Manage a connection that the server encounters.

This function must be called from a child process.

A data block is terminated using the END_DATA_CHAR
A session is terminated using the END_SESSION_CHAR

handler_socket is the socket descriptor returned by the accept() system call.
handler_socket must be closed from outside this function.
*/
void process_connection(int handler_socket)
{
        int bytes_rw;   /* Stores the return values of read() and write() */
        int buf_start;  /* The char-offset to the out_buf */
        int rts;        /* ready to send. Flags when the server is ready to respond */
        char first_char;/* Stores the first character of any data received */
        char in_buf[BUF_LEN], out_buf[BUF_LEN]; /* Input and output buffers */

        sprintf(out_buf, "Hello from server. Please send your data.\n");

        do /* This is the session loop */
        {
                TRACE("Sending a message to the client.");

                bytes_rw = write(handler_socket, out_buf, strlen(out_buf));

                if (bytes_rw < 0)
                {
                        perror("Writing reading client data");
                        TRACE("Clean up the child sockets and exit");
                        close(handler_socket);
                        exit(EXIT_FAILURE);
                }

                buf_start = 0;

                do /* This is the input-data loop */
                {
                        TRACE("Waiting for client data");

                        bytes_rw = read(handler_socket, in_buf, BUF_LEN);

                        if (bytes_rw < 0)
                        {
                                perror("Failed reading client data");
                                TRACE("Clean up the child sockets and exit");
                                close(handler_socket);
                                exit(EXIT_FAILURE);
                        }
                        else if (buf_start + bytes_rw >= BUF_LEN)
                        {
                                perror("Too much data sent from client");
                                TRACE("Clean up the child sockets and exit");
                                close(handler_socket);
                                exit(EXIT_FAILURE);
                        }
                        else
                        {
                                rts = process_client_data(in_buf,
                                                         bytes_rw,
                                                         out_buf,
                                                         BUF_LEN,
                                                         buf_start);
                        }

                        buf_start += bytes_rw;
                        first_char = in_buf[0];
                }
                while (!rts);

                make_lower_alphabetic_only(out_buf, out_buf);
        }
        while (first_char != END_SESSION_CHAR);

        TRACE("Session was successfully ended by client");
}


/*
Copies in to out and scans through client data for terminators.
        - in: input buffer with raw client data of length in_len
        - out: output buffer to copy to, of length out_len
        - offs: the byte-offset when writing to the output buffer

Returns 1 if:
        - END_SESSION_CHAR is the first character; or
        - END_DATA_CHAR is found anywhere in the buffer
Else returns 0 (there is more data to come)
*/
int process_client_data(char *in, int in_len, char *out, int out_len, int offs)
{
        int i;  /* Iterator */

        assert(out_len > in_len + offs);

        if (in[0] == END_SESSION_CHAR)
        {
                return 1;
        }

        for (i = 0; i < in_len; i++)
        {
                if (in[i] == END_DATA_CHAR)
                {
                        out[offs + i] = '\0';   /* Terminate the string */
                        return 1;
                }

                out[offs + i] = in[i];
        }

        out[offs + i] = '\0';   /* Terminate the string */

        return 0;
}


/*
Processes an input string so that it only contains alphabetic characters.

If an upper-case alphabetic character is encountered, it will become lower case.
If a non-alphabetic character except \n is encountered, it will be ommited.

output_str buffer must be equal to or larger in size compared to input_str.
*/
void make_lower_alphabetic_only(char *input_str, char *output_str)
{
        int len = strlen(input_str);
        int out_pos = 0;
        int in_pos;
        char current_char;

        for (in_pos = 0; in_pos < len; in_pos++)
        {
                current_char = input_str[in_pos];

                /* 65-90 is ASCII upper-case alphabetic */
                if (current_char >= 65 && current_char <= 90)
                {
                        /* Add 32 to turn it into lower-case */
                        current_char = current_char + 32;
                }

                /* 97-122 is ASCII lower-case alphabetic */
                if (current_char >= 97 && current_char <= 122)
                {
                        output_str[out_pos] = current_char;
                        out_pos++;
                }

                if (current_char == '\n')
                {
                        output_str[out_pos] = current_char;
                        out_pos++;
                }
        }

        output_str[out_pos] = '\0';
}


/*
Signal handler for SIGCHLD

Collects all child statuses and does nothing with them
in order to prevent zombie processes.
*/
void child_handler(int signum)
{
        int child_pid;

        do
        {
                TRACE("Collecting children");
                child_pid = waitpid(-1, NULL, WNOHANG);
        }
        while (child_pid > 0);
}
