/*
  client.c written by Marcus Belcastro (19185398)

  UNIX socket client for use with server.c.
  This client will loop to receive input from the user and send and receive
  data from the server. Type X to stop execution.

  Compiling instructions:
    gcc client.c -o client.out
*/
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <netdb.h>
#include <string.h>
#include <unistd.h>

#include <sys/types.h>
#include <sys/socket.h>

#define TRACE(message)                                  \
        do                                              \
        {                                               \
                fprintf(stderr, "TRACE: ");             \
                fprintf(stderr, message);               \
                fprintf(stderr, "\n");                  \
        }                                               \
        while(0)
#define BUF_LEN 1024
#define END_SESSION_CHAR 'X'
#define END_DATA_CHAR '&'


/*
  Client that connects to server.c and sends messages for processing.

  Commandline parameters: [HOST] [PORT]
    - HOST: The hostname (or ip address) of the server
    - PORT: The port that the server is listening on
*/
int main(int argc, char *argv[])
{
        int bytes_rw;
        int error_code;
        int client_socket;
        char first_char;
        char recv_buf[BUF_LEN], send_buf[BUF_LEN];
        unsigned short server_port;
        struct hostent *server_host;
        struct sockaddr_in server;

        if (argc < 3)
        {
                printf("Invalid arguments, use: [HOST] [PORT]\n");
                exit(EXIT_FAILURE);
        }

        server_host = gethostbyname(argv[1]);
        if (!server_host)
        {
                perror("Could not find the host specified.");
                exit(EXIT_FAILURE);
        }

        server_port = atoi(argv[2]);

        TRACE("Creating the client socket");

        server.sin_family = AF_INET;
        server.sin_port = htons(server_port);
        memcpy(&server.sin_addr.s_addr,
                server_host->h_addr,
                server_host->h_length
        );

        client_socket = socket(AF_INET, SOCK_STREAM, 0);
        if (client_socket < 0)
        {
                perror("Failed to create a socket.");
                exit(EXIT_SUCCESS);
        }

        TRACE("Successfully created the socket, now connecting");

        error_code = connect(client_socket,
                                (struct sockaddr*) &server,
                                sizeof(server)
        );

        if (error_code < 0)
        {
                perror("Failed to connect to the socket.");
                exit(EXIT_SUCCESS);
        }

        TRACE("Successfully connected to the socket, running the main loop");

        do
        {
                TRACE("Awaiting server message");

                memset(recv_buf, 0, BUF_LEN);

                bytes_rw = read(client_socket, recv_buf, BUF_LEN);

                if (bytes_rw < 0)
                {
                        perror("Error reading from the socket");
                        close(client_socket);
                        exit(EXIT_FAILURE);
                }

                printf("The server sent this message: %s\n", recv_buf);

                printf("Type your message to send (X to stop):\n");
                /* WARNING: Buffer overflow not protected on user input */
                fgets(send_buf, BUF_LEN, stdin);

                first_char = send_buf[0];

                if (first_char != END_SESSION_CHAR)
                {
                        /* Append the terminating END_DATA_CHAR to the message */
                        bytes_rw = strlen(send_buf);
                        send_buf[bytes_rw] = '\0';
                        send_buf[bytes_rw - 1] = END_DATA_CHAR;
                }

                bytes_rw = write(client_socket, send_buf, strlen(send_buf));

                if (bytes_rw < 0)
                {
                        perror("Error writing to the socket");
                        close(client_socket);
                        exit(EXIT_FAILURE);
                }
        }
        while (first_char != END_SESSION_CHAR);

        TRACE("Successfully exiting the client");

        close(client_socket);

        return EXIT_SUCCESS;
}
