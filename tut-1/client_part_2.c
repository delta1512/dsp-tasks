/*
  client_part_2.c written by Marcus Belcastro (19185398)

  UNIX socket client for use with server.c. This version uses the lower-level
  socket interface with the connect() method and read/write system calls.
  The client uses the UDP transport layer protocol.

  Compiling instructions:
    gcc client_part_2.c -o client.out
*/
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <netdb.h>
#include <string.h>

#include <netinet/in.h>

#include <sys/types.h>
#include <sys/socket.h>

#define TRACE(message) do {fprintf(stderr, "TRACE: ");fprintf(stderr, message);} while(0)
#define BUF_LEN 1024


/*
  Client that connects to server.c and sends a string to be modified.

  Commandline parameters: [HOST] [PORT] [MESSAGE]
    - HOST: The hostname (or ip address) of the server
    - PORT: The port that the server is listening on
    - MESSAGE: The message to send to the server
*/
int main(int argc, char *argv[])
{
  int client_socket;
  int recv_bytes;
  int sent_bytes;
  char recv_buf[BUF_LEN];
  char send_buf[BUF_LEN];
  unsigned short server_port;
  struct hostent *server_host;
  struct sockaddr_in server;
  socklen_t server_len = sizeof(server);

  if (argc < 4)
  {
    perror("Invalid arguments, use: [HOST] [PORT] [MESSAGE]\n");
    exit(EXIT_FAILURE);
  }

  server_host = gethostbyname(argv[1]);
  if (!server_host)
  {
      perror("Could not find the host specified.\n");
      exit(EXIT_FAILURE);
  }

  server_port = atoi(argv[2]);
  if (server_port <= 1024)
  {
    perror("Bad port specified, use port numbers higher than 1024.\n");
    exit(EXIT_FAILURE);
  }

  strcpy(send_buf, argv[3]);

  TRACE("Validated all inputs, creating socket...\n");

  server.sin_family = PF_INET;
  server.sin_port = htons(server_port);
  memcpy(&server.sin_addr.s_addr, server_host->h_addr, server_host->h_length);

  client_socket = socket(PF_INET, SOCK_DGRAM, 0);
  if (client_socket < 0)
  {
    perror("Failed to create a socket.\n");
    exit(EXIT_SUCCESS);
  }

  TRACE("Successfully created the socket, now connecting...\n");

  int success_connect = connect(
    client_socket,
    (struct sockaddr*) &server,
    server_len
  );

  if (success_connect < 0)
  {
    perror("Failed to connect to the socket.\n");
    exit(EXIT_SUCCESS);
  }

  TRACE("Successfully connected to the socket\n");

  fprintf(stderr, "Sending your message: %s\n", send_buf);

  sent_bytes = write(client_socket, send_buf, BUF_LEN);

  if (sent_bytes <= 0)
  {
    perror("Failed to send the message to the server.\n");
    exit(EXIT_FAILURE);
  }

  TRACE("Sent the message successfully, now waiting for response...\n");

  recv_bytes = read(client_socket, recv_buf, BUF_LEN);

  if (recv_bytes <= 0)
  {
    perror("Failed to receive message from the server.\n");
    exit(EXIT_FAILURE);
  }

  fprintf(stderr, "Message from the server: %s\n", recv_buf);

  close(client_socket);

  TRACE("Closed the client successfully!\n");

  return EXIT_SUCCESS;
}
