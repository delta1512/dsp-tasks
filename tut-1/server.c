/*
  server.c written by Marcus Belcastro (19185398)

  UNIX socket server for use with client.c and client_part_2.c.
  The server will await messages, modify them and send them back to the client.
  The server uses the UDP transport layer protocol.

  Compiling instructions:
    gcc server.c -o server.out
*/
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>

#include <netinet/in.h>

#include <sys/types.h>
#include <sys/socket.h>

#define TRACE(message) fprintf(stderr, "TRACE: ");fprintf(stderr, message)
#define BUF_LEN 1024
#define INFINITE_REPLY_SENTINEL -1


void make_lower_alphabetic_only(char *input_str, char *output_str);

/*
  Server that accepts connections and modifies the text that was received.
  Accessible by running client.c.

  Commandline parameters: [PORT] [MAX REPLIES]
    - PORT: The port that the server will run on
    - MAX REPLIES: The amount of replies the server will serve (0 means infinity)
*/
int main(int argc, char *argv[])
{
  int replies;
  int server_socket;
  unsigned short server_port;
  struct sockaddr_in server;
  struct sockaddr_in client;
  socklen_t client_len = sizeof(client);

  if (argc < 3)
  {
    perror("Invalid arguments, use: [PORT] [MAX REPLIES]\n");
    exit(EXIT_FAILURE);
  }

  server_port = atoi(argv[1]);
  if (server_port <= 1024)
  {
    perror("Bad port specified, use port numbers higher than 1024.\n");
    exit(EXIT_FAILURE);
  }

  replies = atoi(argv[2]);
  if (replies <= 0)
  {
    replies = INFINITE_REPLY_SENTINEL;
  }

  TRACE("Validated all inputs, creating socket...\n");

  server.sin_family = PF_INET;
  server.sin_port = htons(server_port);
  server.sin_addr.s_addr = htonl(INADDR_ANY);

  server_socket = socket(PF_INET, SOCK_DGRAM, 17);
  if (server_socket < 0)
  {
    perror("Failed to create a socket.\n");
    exit(EXIT_SUCCESS);
  }

  TRACE("Binding the socket...\n");

  int bind_success = bind(server_socket, &server, sizeof(server));
  if (bind_success < 0)
  {
    perror("Failed to bind the socket to the address and port specified.\n");
    exit(EXIT_FAILURE);
  }

  TRACE("Entering the server loop...\n");

  for (int i = replies; (i > 0 || replies == INFINITE_REPLY_SENTINEL); i--)
  {
    int recv_bytes, sent_bytes;
    char recv_buf[BUF_LEN];
    char send_buf[BUF_LEN];

    TRACE("Waiting for a new connection...\n");

    recv_bytes = recvfrom(
      server_socket,
      recv_buf,
      BUF_LEN,
      0,
      (struct sockaddr*) &client,
      &client_len
    );

    if (recv_bytes < 0)
    {
      perror("Bad connection accepted, skipping.\n");
      continue;
    }

    fprintf(stderr, "Connection accepted, message is %d bytes\n", recv_bytes);
    fprintf(stderr, "Message is: %s\n", recv_buf);

    make_lower_alphabetic_only(recv_buf, send_buf);

    TRACE("Sending modified message...\n");

    sent_bytes = sendto(
      server_socket,
      send_buf,
      strlen(send_buf) + 1, /* only send the actual string part of buffer */
      0,
      (struct sockaddr*) &client,
      client_len
    );

    fprintf(stderr, "Modified the message is %d bytes\n", sent_bytes);
    fprintf(stderr, "Modified is %s\n", send_buf);
  }

  close(server_socket);

  TRACE("Closed the server successfully!\n");

  return EXIT_SUCCESS;
}


/*
  Processes an input string so that it only contains alphabetic characters.

  If an upper-case alphabetic character is encountered, it will become lower case.
  If a non-alphabetic character is encountered, it will be ommited.
*/
void make_lower_alphabetic_only(char *input_str, char *output_str)
{
  int len = strlen(input_str);
  int out_pos = 0;

  for (int in_pos = 0; in_pos < len; in_pos++)
  {
    char current_char = input_str[in_pos];

    /* 65-90 is ASCII upper-case alphabetic */
    if (current_char >= 65 && current_char <= 90)
    {
      /* Add 32 to turn it into lower-case */
      current_char = current_char + 32;
    }

    /* 97-122 is ASCII lower-case alphabetic */
    if (current_char >= 97 && current_char <= 122)
    {
      output_str[out_pos] = current_char;
      out_pos++;
    }
  }

  output_str[out_pos] = '\0';
}
